package com.company.newamplproject.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import com.company.newamplproject.insurance.InsurancePolicy;
import com.company.newamplproject.insurance.InsurancePolicyRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Controller
public class InsurancePolicyController {
    private final InsurancePolicyRepository crudRepository;

    public InsurancePolicyController(InsurancePolicyRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @QueryMapping(name = "countInsurancePolicy")
    @Transactional(readOnly = true)
    public long count() {
        return crudRepository.count();
    }

    @MutationMapping(name = "deleteInsurancePolicy")
    @Transactional
    public void delete(@GraphQLId @Argument @NonNull Long id) {
        InsurancePolicy entity = crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "insurancePolicyList")
    @Transactional(readOnly = true)
    @NonNull
    public List<InsurancePolicy> findAll() {
        return crudRepository.findAll();
    }

    @QueryMapping(name = "insurancePolicy")
    @Transactional(readOnly = true)
    @NonNull
    public InsurancePolicy findById(@GraphQLId @Argument @NonNull Long id) {
        return crudRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateInsurancePolicy")
    @Transactional
    @NonNull
    public InsurancePolicy update(@Argument @NonNull InsurancePolicy input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new RuntimeException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        return crudRepository.save(input);
    }
}