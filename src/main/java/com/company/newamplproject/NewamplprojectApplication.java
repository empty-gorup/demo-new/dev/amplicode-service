package com.company.newamplproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class NewamplprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewamplprojectApplication.class, args);
    }
}
