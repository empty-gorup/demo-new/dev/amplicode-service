package com.company.newamplproject.insurance;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "insurance_policy")
public class InsurancePolicy {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "policy_level")
    private PolicyLevel policyLevel;

    @Column(name = "notes")
    private String notes;

    @Column(name = "expires_at")
    private LocalDate expiresAt;

    @Column(name = "number")
    private String number;

    public PolicyLevel getPolicyLevel() {
        return policyLevel;
    }

    public void setPolicyLevel(PolicyLevel policyLevel) {
        this.policyLevel = policyLevel;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalDate getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(LocalDate expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}