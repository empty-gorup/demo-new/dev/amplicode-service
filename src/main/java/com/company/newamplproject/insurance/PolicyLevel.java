package com.company.newamplproject.insurance;

public enum PolicyLevel {
    BASIC, PREMIUM
}
