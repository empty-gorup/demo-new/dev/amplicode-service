import { TranslationMessages } from "ra-core";
import englishMessages from "ra-language-english";

export const en: TranslationMessages = {
  ...englishMessages,

  resources: {
    InsurancePolicy: {
      name: "InsurancePolicy |||| InsurancePolicies",

      fields: {
        expiresAt: "Expires At",
        notes: "Notes",
        number: "Number",
        policyLevel: "Policy Level"
      }
    }
  },

  enums: {
    PolicyLevel: {
      BASIC: "Basic",
      PREMIUM: "Premium"
    }
  }
};
