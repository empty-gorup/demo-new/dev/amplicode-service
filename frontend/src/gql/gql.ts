/* eslint-disable */
import * as types from "./graphql";
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
  "mutation UpdateInsurance($input: InsurancePolicyInput!) {\n  updateInsurance(input: $input) {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}":
    types.UpdateInsuranceDocument,
  "query Insurance($id: ID!) {\n  insurance(id: $id) {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}":
    types.InsuranceDocument,
  "query InsurancePolicies {\n  insurancePolicies {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}":
    types.InsurancePoliciesDocument,
  "mutation DeleteInsurance($id: ID!) {\n  deleteInsurance(id: $id) \n}":
    types.DeleteInsuranceDocument,
  "\n     query userPermissions {\n         userPermissions\n     }\n":
    types.UserPermissionsDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function gql(source: string): unknown;

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "mutation UpdateInsurance($input: InsurancePolicyInput!) {\n  updateInsurance(input: $input) {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}"
): (typeof documents)["mutation UpdateInsurance($input: InsurancePolicyInput!) {\n  updateInsurance(input: $input) {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "query Insurance($id: ID!) {\n  insurance(id: $id) {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}"
): (typeof documents)["query Insurance($id: ID!) {\n  insurance(id: $id) {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "query InsurancePolicies {\n  insurancePolicies {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}"
): (typeof documents)["query InsurancePolicies {\n  insurancePolicies {\n    expiresAt\n    id\n    notes\n    number\n    policyLevel\n  }\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "mutation DeleteInsurance($id: ID!) {\n  deleteInsurance(id: $id) \n}"
): (typeof documents)["mutation DeleteInsurance($id: ID!) {\n  deleteInsurance(id: $id) \n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "\n     query userPermissions {\n         userPermissions\n     }\n"
): (typeof documents)["\n     query userPermissions {\n         userPermissions\n     }\n"];

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> =
  TDocumentNode extends DocumentNode<infer TType, any> ? TType : never;
