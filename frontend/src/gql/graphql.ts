/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigDecimal: any;
  BigInteger: any;
  Date: any;
  DateTime: any;
  LocalDateTime: any;
  LocalTime: any;
  Long: any;
  Time: any;
  Timestamp: any;
  Url: any;
  Void: any;
};

export type InsurancePolicy = {
  __typename?: "InsurancePolicy";
  expiresAt?: Maybe<Scalars["Date"]>;
  id?: Maybe<Scalars["ID"]>;
  notes?: Maybe<Scalars["String"]>;
  number?: Maybe<Scalars["String"]>;
  policyLevel?: Maybe<PolicyLevel>;
};

export type InsurancePolicyInput = {
  expiresAt?: InputMaybe<Scalars["Date"]>;
  id?: InputMaybe<Scalars["ID"]>;
  notes?: InputMaybe<Scalars["String"]>;
  number?: InputMaybe<Scalars["String"]>;
  policyLevel?: InputMaybe<PolicyLevel>;
};

export type Mutation = {
  __typename?: "Mutation";
  deleteInsurance?: Maybe<Scalars["Void"]>;
  deleteInsurancePolicy?: Maybe<Scalars["Void"]>;
  updateInsurance: InsurancePolicy;
  updateInsurancePolicy: InsurancePolicy;
};

export type MutationDeleteInsuranceArgs = {
  id: Scalars["ID"];
};

export type MutationDeleteInsurancePolicyArgs = {
  id: Scalars["ID"];
};

export type MutationUpdateInsuranceArgs = {
  input: InsurancePolicyInput;
};

export type MutationUpdateInsurancePolicyArgs = {
  input: InsurancePolicyInput;
};

export enum PolicyLevel {
  Basic = "BASIC",
  Premium = "PREMIUM",
}

export type Query = {
  __typename?: "Query";
  countInsurancePolicy: Scalars["Long"];
  insurance: InsurancePolicy;
  insurancePolicies?: Maybe<Array<Maybe<InsurancePolicy>>>;
  insurancePolicy: InsurancePolicy;
  insurancePolicyList?: Maybe<Array<Maybe<InsurancePolicy>>>;
  userInfo?: Maybe<UserInfo>;
  userPermissions?: Maybe<Array<Maybe<Scalars["String"]>>>;
};

export type QueryInsuranceArgs = {
  id: Scalars["ID"];
};

export type QueryInsurancePolicyArgs = {
  id: Scalars["ID"];
};

export type UserInfo = {
  __typename?: "UserInfo";
  username?: Maybe<Scalars["String"]>;
};

export type UpdateInsuranceMutationVariables = Exact<{
  input: InsurancePolicyInput;
}>;

export type UpdateInsuranceMutation = {
  __typename?: "Mutation";
  updateInsurance: {
    __typename?: "InsurancePolicy";
    expiresAt?: any | null;
    id?: string | null;
    notes?: string | null;
    number?: string | null;
    policyLevel?: PolicyLevel | null;
  };
};

export type InsuranceQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type InsuranceQuery = {
  __typename?: "Query";
  insurance: {
    __typename?: "InsurancePolicy";
    expiresAt?: any | null;
    id?: string | null;
    notes?: string | null;
    number?: string | null;
    policyLevel?: PolicyLevel | null;
  };
};

export type InsurancePoliciesQueryVariables = Exact<{ [key: string]: never }>;

export type InsurancePoliciesQuery = {
  __typename?: "Query";
  insurancePolicies?: Array<{
    __typename?: "InsurancePolicy";
    expiresAt?: any | null;
    id?: string | null;
    notes?: string | null;
    number?: string | null;
    policyLevel?: PolicyLevel | null;
  } | null> | null;
};

export type DeleteInsuranceMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type DeleteInsuranceMutation = {
  __typename?: "Mutation";
  deleteInsurance?: any | null;
};

export type UserPermissionsQueryVariables = Exact<{ [key: string]: never }>;

export type UserPermissionsQuery = {
  __typename?: "Query";
  userPermissions?: Array<string | null> | null;
};

export const UpdateInsuranceDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "UpdateInsurance" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "InsurancePolicyInput" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateInsurance" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "expiresAt" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "notes" } },
                { kind: "Field", name: { kind: "Name", value: "number" } },
                { kind: "Field", name: { kind: "Name", value: "policyLevel" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UpdateInsuranceMutation,
  UpdateInsuranceMutationVariables
>;
export const InsuranceDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Insurance" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "insurance" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "expiresAt" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "notes" } },
                { kind: "Field", name: { kind: "Name", value: "number" } },
                { kind: "Field", name: { kind: "Name", value: "policyLevel" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<InsuranceQuery, InsuranceQueryVariables>;
export const InsurancePoliciesDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "InsurancePolicies" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "insurancePolicies" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "expiresAt" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "notes" } },
                { kind: "Field", name: { kind: "Name", value: "number" } },
                { kind: "Field", name: { kind: "Name", value: "policyLevel" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  InsurancePoliciesQuery,
  InsurancePoliciesQueryVariables
>;
export const DeleteInsuranceDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "DeleteInsurance" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteInsurance" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  DeleteInsuranceMutation,
  DeleteInsuranceMutationVariables
>;
export const UserPermissionsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "userPermissions" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "userPermissions" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UserPermissionsQuery,
  UserPermissionsQueryVariables
>;
