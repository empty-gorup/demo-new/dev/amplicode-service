import { DevSupport } from "@react-buddy/ide-toolbox";
import { AdminContext, AdminUI, Loading, Resource } from "react-admin";
import { useAuthProvider } from "../authProvider/useAuthProvider";
import { getInsurancePolicyRecordRepresentation } from "../core/record-representation/getInsurancePolicyRecordRepresentation";
import { dataProvider } from "../dataProvider/graphqlDataProvider";
import { ComponentPreviews, useInitial } from "../dev";
import { i18nProvider } from "../i18nProvider";
import { InsuranceCreate } from "..\\app\\screens\\insurance-policies\\InsuranceCreate";
import { InsuranceEdit } from "..\\app\\screens\\insurance-policies\\InsuranceEdit";
import { InsurancePoliciesList } from "..\\app\\screens\\insurance-policies\\InsurancePoliciesList";
import { AdminLayout } from "./AdminLayout";

export const App = () => {
  const { authProvider, loading } = useAuthProvider();

  if (loading) {
    return (
      <Loading
        loadingPrimary="Loading"
        loadingSecondary="The page is loading, just a moment please"
      />
    );
  }

  return (
    <AdminContext
      dataProvider={dataProvider}
      authProvider={authProvider}
      i18nProvider={i18nProvider}
    >
      <DevSupport ComponentPreviews={ComponentPreviews} useInitialHook={useInitial}>
        <AdminUI layout={AdminLayout}>
          <Resource
            name="InsurancePolicy"
            options={{ label: "Insurance Policy" }}
            list={InsurancePoliciesList}
            recordRepresentation={getInsurancePolicyRecordRepresentation}
            create={InsuranceCreate}
            edit={InsuranceEdit}
          />
        </AdminUI>
      </DevSupport>
    </AdminContext>
  );
};
