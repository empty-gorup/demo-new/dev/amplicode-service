import { gql } from "@amplicode/gql";
import { InsurancePolicy, PolicyLevel } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { Datagrid, DeleteButton, EditButton, FunctionField, List, TextField } from "react-admin";
import { EnumField } from "../../../core/fields/EnumField";
import { renderDate } from "../../../core/format/renderDate";

const INSURANCE_POLICIES = gql(`query InsurancePolicies {
  insurancePolicies {
    expiresAt
    id
    notes
    number
    policyLevel
  }
}`);

const DELETE_INSURANCE = gql(`mutation DeleteInsurance($id: ID!) {
  deleteInsurance(id: $id) 
}`);

export const InsurancePoliciesList = () => {
  const queryOptions = {
    meta: {
      query: INSURANCE_POLICIES,
      resultDataPath: "",
    },
  };

  return (
    <List<ItemType> queryOptions={queryOptions} exporter={false} pagination={false}>
      <Datagrid rowClick="show" bulkActionButtons={false}>
        <TextField source="id" sortable={false} />

        <FunctionField
          source="expiresAt"
          render={(record: InsurancePolicy) => renderDate(record.expiresAt)}
        />
        <TextField source="notes" sortable={false} />
        <TextField source="number" sortable={false} />
        <EnumField
          source="policyLevel"
          enumTypeName="PolicyLevel"
          enum={PolicyLevel}
          sortable={false}
        />

        <EditButton />
        <DeleteButton
          mutationMode="pessimistic"
          mutationOptions={{ meta: { mutation: DELETE_INSURANCE } }}
        />
      </Datagrid>
    </List>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof INSURANCE_POLICIES>;
/**
 * Type of the items list
 */
type ItemListType = QueryResultType["insurancePolicies"];
/**
 * Type of single item
 */
type ItemType = { id: string } & Exclude<Exclude<ItemListType, null | undefined>[0], undefined>;
