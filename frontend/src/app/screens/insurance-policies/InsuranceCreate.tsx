import { gql } from "@amplicode/gql";
import { PolicyLevel } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { useCallback } from "react";
import {
  Create,
  DateInput,
  SimpleForm,
  TextInput,
  useCreate,
  useNotify,
  useRedirect,
} from "react-admin";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { checkServerValidationErrors } from "../../../core/error/checkServerValidationError";
import { EnumInput } from "../../../core/inputs/EnumInput";

const UPDATE_INSURANCE = gql(`mutation UpdateInsurance($input: InsurancePolicyInput!) {
  updateInsurance(input: $input) {
    expiresAt
    id
    notes
    number
    policyLevel
  }
}`);

export const InsuranceCreate = () => {
  const redirect = useRedirect();
  const notify = useNotify();
  const [create] = useCreate();

  const save: SubmitHandler<FieldValues> = useCallback(
    async (data: FieldValues) => {
      try {
        const params = { data, meta: { mutation: UPDATE_INSURANCE } };
        const options = { returnPromise: true };

        await create("InsurancePolicy", params, options);

        notify("ra.notification.created", { messageArgs: { smart_count: 1 } });
        redirect("list", "InsurancePolicy");
      } catch (response: any) {
        console.log("create failed with error", response);
        return checkServerValidationErrors(response, notify);
      }
    },
    [create, notify, redirect]
  );

  return (
    <Create<ItemType> redirect="list">
      <SimpleForm onSubmit={save}>
        <DateInput source="expiresAt" name="expiresAt" />
        <TextInput source="notes" name="notes" />
        <TextInput source="number" name="number" />
        <EnumInput
          name="policyLevel"
          source="policyLevel"
          enumTypeName="PolicyLevel"
          enum={PolicyLevel}
        />
      </SimpleForm>
    </Create>
  );
};

const INSURANCE_TYPE = gql(`query Insurance($id: ID!) {
  insurance(id: $id) {
    expiresAt
    id
    notes
    number
    policyLevel
  }
}`);

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof INSURANCE_TYPE>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = { id: string } & Exclude<QueryResultType["insurance"], undefined>;
