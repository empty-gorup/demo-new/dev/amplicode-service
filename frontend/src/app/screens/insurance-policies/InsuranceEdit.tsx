import { gql } from "@amplicode/gql";
import { PolicyLevel } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { useCallback } from "react";
import {
  DateInput,
  Edit,
  SimpleForm,
  TextInput,
  useNotify,
  useRedirect,
  useUpdate,
} from "react-admin";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { checkServerValidationErrors } from "../../../core/error/checkServerValidationError";
import { EnumInput } from "../../../core/inputs/EnumInput";

const INSURANCE = gql(`query Insurance($id: ID!) {
  insurance(id: $id) {
    expiresAt
    id
    notes
    number
    policyLevel
  }
}`);
const UPDATE_INSURANCE = gql(`mutation UpdateInsurance($input: InsurancePolicyInput!) {
  updateInsurance(input: $input) {
    expiresAt
    id
    notes
    number
    policyLevel
  }
}`);

export const InsuranceEdit = () => {
  const queryOptions = {
    meta: {
      query: INSURANCE,
      resultDataPath: null,
    },
  };

  const redirect = useRedirect();
  const notify = useNotify();
  const [update] = useUpdate();

  const save: SubmitHandler<FieldValues> = useCallback(
    async (data: FieldValues) => {
      try {
        const params = { data, meta: { mutation: UPDATE_INSURANCE } };
        const options = { returnPromise: true };

        await update("InsurancePolicy", params, options);

        notify("ra.notification.updated", { messageArgs: { smart_count: 1 } });
        redirect("list", "InsurancePolicy");
      } catch (response: any) {
        console.log("update failed with error", response);
        return checkServerValidationErrors(response, notify);
      }
    },
    [update, notify, redirect]
  );

  return (
    <Edit<ItemType> mutationMode="pessimistic" queryOptions={queryOptions}>
      <SimpleForm onSubmit={save}>
        <DateInput source="expiresAt" name="expiresAt" />
        <TextInput source="notes" name="notes" />
        <TextInput source="number" name="number" />
        <EnumInput
          name="policyLevel"
          source="policyLevel"
          enumTypeName="PolicyLevel"
          enum={PolicyLevel}
        />
      </SimpleForm>
    </Edit>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof INSURANCE>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = { id: string } & Exclude<QueryResultType["insurance"], undefined>;
